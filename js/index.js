const marker1 = document.getElementById("red-marker-1");
const marker2 = document.getElementById("red-marker-2");
const marker3 = document.getElementById("red-marker-3");
const marker4 = document.getElementById("red-marker-4");
const marker5 = document.getElementById("red-marker-5");
const marker6 = document.getElementById("red-marker-6");

const list1 = document.getElementById("list-1");
const list2 = document.getElementById("list-2");
const list3 = document.getElementById("list-3");
const list4 = document.getElementById("list-4");
const list5 = document.getElementById("list-5");
const list6 = document.getElementById("list-6");

const listElements = [
  { marker: marker1, li: list1, top: "0%", left: "59%" },
  { marker: marker2, li: list2, top: "83%", left: "44%" },
  { marker: marker3, li: list3, top: "54%", left: "12%" },
  { marker: marker4, li: list4, top: "37%", left: "70%" },
  { marker: marker5, li: list5, top: "50%", left: "80%" },
  { marker: marker6, li: list6, top: "93%", left: "82%" }
];

const servicesList = document.getElementById("services-list");

setOnSpanHoverEvents(listElements);
setOnListHoverEvents(servicesList);

let interval = setInterval(switchListElement, 4000);
let counter = listElements.length - 1;

function setOnSpanHoverEvents(listElements) {
  for (let i = 0; i < listElements.length; i++) {
    const { li, marker, top, left } = listElements[i];

    li.addEventListener("mouseover", function () {
      marker.style.opacity = 1;
      marker.style.top = top;
      marker.style.left = left;
      li.style.borderBottomColor = "#F53F3F";
    });

    li.addEventListener("mouseout", function () {
      marker.style.opacity = 0;
      li.style.borderBottomColor = "#F5F5F5";
    });
  }
}

function setOnListHoverEvents(servicesList) {
  servicesList.addEventListener("mouseover", function () {
    clearInterval(interval);
  });

  servicesList.addEventListener("mouseout", function () {
    interval = setInterval(switchListElement, 4000);
  });
}

function switchListElement() {
  const current = listElements[counter];
  hideElement(current);

  if (counter === listElements.length - 1) counter = 0;
  else counter++;

  const next = listElements[counter];
  showElement(next);
}

function showElement(el) {
  el.marker.style.opacity = 1;
  el.marker.style.top = el.top;
  el.marker.style.left = el.left;
  el.li.style.borderBottomColor = "#F53F3F";
}

function hideElement(el) {
  el.marker.style.opacity = 0;
  el.li.style.borderBottomColor = "#F5F5F5";
}
